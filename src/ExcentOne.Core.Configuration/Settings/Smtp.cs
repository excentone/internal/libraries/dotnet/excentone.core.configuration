﻿using System.Collections.Generic;
using System.ComponentModel;

namespace ExcentOne.Core.Configuration.Settings
{
    public class Smtp
    {
        public string UseSettingName { get; set; }

        public string SettingName { get; set; }

        [DefaultValue(false)] public bool UseDefaultCredentials { get; set; }

        [DefaultValue(false)] public bool UseLocal { get; set; }

        [DefaultValue("smtp.office365.com")] public string Host { get; set; }

        [DefaultValue(587)] public int Port { get; set; }

        public string Username { get; set; }

        public string EncryptedPassword { get; set; }

        [DefaultValue(true)]
        public bool EnableSsl { get; set; }

        public string Domain { get; set; }

        [DefaultValue("STARTTLS/smtp.office365.com")]
        public string TargetName { get; set; }

        protected internal void ReInitialize(Smtps smtps)
        {
            if (UseSettingName == null || UseSettingName.Equals(string.Empty)) return;

            foreach (var smtp in smtps)
            {
                if (!smtp.SettingName.ToLower().Trim().Equals(UseSettingName.ToLower().Trim())) continue;

                UseDefaultCredentials = smtp.UseDefaultCredentials;
                UseLocal = smtp.UseLocal;
                Host = smtp.Host;
                Port = smtp.Port;
                Username = smtp.Username;
                EncryptedPassword = smtp.EncryptedPassword;
                EnableSsl = smtp.EnableSsl;
                Domain = smtp.Domain;
                TargetName = smtp.TargetName;
            }
        }
    }

    public class Smtps : List<Smtp>
    {
        private const string DefaultUseSettingName = "DefaultSetting";

        public Smtp DefaultConnection
        {
            get
            {
                var defaultConnection = Find(c =>
                    c.SettingName.ToLowerInvariant().Equals(DefaultUseSettingName.ToLowerInvariant()));

                if (defaultConnection != null) return defaultConnection;
                if (Count > 1) defaultConnection = this[0];
                return defaultConnection;
            }
        }

        public Smtp GetSettingByName(string settingName)
        {
            return Find(c => c.SettingName.ToLowerInvariant().Equals(settingName.ToLowerInvariant()));
        }

        public void ReInitialize()
        {
            foreach (var connection in this) connection.ReInitialize(this);
        }
    }
}