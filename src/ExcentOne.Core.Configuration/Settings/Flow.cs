﻿using System.ComponentModel;

namespace ExcentOne.Core.Configuration.Settings
{
    public class Flow
    {
        [DefaultValue("")]
        public string UseConnectionName { get; set; }

        [DefaultValue("localhost")]
        public string DataSource { get; set; }

        [DefaultValue("EOFLOWDB")]
        public string InitialCatalog { get; set; }

        [DefaultValue(true)]
        public bool IsPersistSecurityInfo { get; set; }

        public string UserId { get; set; }

        public string EncryptedPassword { get; set; }

        protected internal void ReInitialize(Connections connections)
        {
            if (UseConnectionName == null || UseConnectionName.Equals(string.Empty)) return;

            foreach (var connection in connections)
            {
                if (!connection.ConnectionName.ToLower().Trim().Equals(UseConnectionName.ToLower().Trim())) continue;

                DataSource = connection.DataSource;
                InitialCatalog = connection.InitialCatalog;
                IsPersistSecurityInfo = connection.IsPersistSecurityInfo;
                UserId = connection.UserId;
                EncryptedPassword = connection.EncryptedPassword;
            }
        }
    }
}