﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using Npgsql;

namespace ExcentOne.Core.Configuration.Settings
{
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public class Connection
    {
        public string UseConnectionName { get; set; }

        public string ConnectionName { get; set; }

        [DefaultValue("MSSQL")] public string ConnectionType { get; set; }

        [DefaultValue("localhost")] public string DataSource { get; set; }

        public string InitialCatalog { get; set; }

        [DefaultValue(true)] public bool IsPersistSecurityInfo { get; set; }

        public string UserId { get; set; }

        public string EncryptedPassword { get; set; }

        [DefaultValue(false)] public bool MultipleActiveResultSets { get; set; }

        [DefaultValue(30)] public int ConnectTimeout { get; set; }

        public bool IntegratedSecurity { get; set; }

        [DefaultValue(true)]
        public bool Pooling { get; set; }

        public string OtherSettings { get; set; }

        //Azure DB Settings
        [DefaultValue(true)] public bool Encrypt { get; set; }

        [DefaultValue(false)] public bool TrustServerCertificate { get; set; }

        //Postgresql Settings
        public int Port { get; set; }

        public string ConnectionString
        {
            get
            {
                if (ConnectionType.ToLowerInvariant().Equals("mssql"))
                    return new SqlConnectionStringBuilder
                    {
                        DataSource = DataSource,
                        UserID = UserId,
                        Password = EncryptedPassword,
                        InitialCatalog = InitialCatalog,
                        PersistSecurityInfo = IsPersistSecurityInfo,
                        MultipleActiveResultSets = MultipleActiveResultSets,
                        ConnectTimeout = ConnectTimeout,
                        IntegratedSecurity = IntegratedSecurity,
                        Pooling = Pooling
                    }.ConnectionString;

                if (ConnectionType.ToLowerInvariant().Equals("azuredb"))
                    return new SqlConnectionStringBuilder
                    {
                        DataSource = DataSource,
                        InitialCatalog = InitialCatalog,
                        PersistSecurityInfo = IsPersistSecurityInfo,
                        UserID = UserId,
                        Password = EncryptedPassword,
                        MultipleActiveResultSets = MultipleActiveResultSets,
                        Encrypt = Encrypt,
                        TrustServerCertificate = TrustServerCertificate,
                        ConnectTimeout = ConnectTimeout,
                        Pooling = Pooling
                    }.ConnectionString;

                if (ConnectionType.ToLowerInvariant().Equals("postgresql"))
                    return new NpgsqlConnectionStringBuilder
                    {
                        Host = DataSource,
                        Database = InitialCatalog,
                        Username = UserId,
                        Password = EncryptedPassword,
                        Port = Port,
                        Pooling = Pooling
                    }.ConnectionString;

                throw new NotImplementedException($"{ConnectionType} is not supported");
            }
        }

        protected internal void ReInitialize(Connections connections)
        {
            if (UseConnectionName == null || UseConnectionName.Equals(string.Empty)) return;

            foreach (var connection in connections)
            {
                if (!connection.ConnectionName.ToLower().Trim().Equals(UseConnectionName.ToLower().Trim())) continue;

                ConnectionType = connection.ConnectionType ?? GetConnectionType(UseConnectionName, connections);
                if (connection.ConnectionType == null) continue;

                if (connection.ConnectionType.ToLowerInvariant().Equals("mssql"))
                {
                    DataSource = connection.DataSource;
                    InitialCatalog = connection.InitialCatalog;
                    IsPersistSecurityInfo = connection.IsPersistSecurityInfo;
                    UserId = connection.UserId;
                    EncryptedPassword = connection.EncryptedPassword;
                    MultipleActiveResultSets = connection.MultipleActiveResultSets;
                    ConnectTimeout = connection.ConnectTimeout;
                    IntegratedSecurity = connection.IntegratedSecurity;
                    Pooling = connection.Pooling;
                }
                else if (connection.ConnectionType.ToLowerInvariant().Equals("azuredb"))
                {
                    DataSource = connection.DataSource;
                    InitialCatalog = connection.InitialCatalog;
                    IsPersistSecurityInfo = connection.IsPersistSecurityInfo;
                    UserId = connection.UserId;
                    EncryptedPassword = connection.EncryptedPassword;
                    MultipleActiveResultSets = connection.MultipleActiveResultSets;
                    Encrypt = connection.Encrypt;
                    TrustServerCertificate = connection.TrustServerCertificate;
                    ConnectTimeout = connection.ConnectTimeout;
                    Pooling = connection.Pooling;
                }
                else if (connection.ConnectionType.ToLowerInvariant().Equals("postgresql"))
                {
                    DataSource = connection.DataSource;
                    InitialCatalog = connection.InitialCatalog;
                    UserId = connection.UserId;
                    EncryptedPassword = connection.EncryptedPassword;
                    Port = connection.Port;
                    Pooling = connection.Pooling;
                }
                else
                {
                    DataSource = connection.DataSource;
                    InitialCatalog = connection.InitialCatalog;
                    IsPersistSecurityInfo = connection.IsPersistSecurityInfo;
                    UserId = connection.UserId;
                    EncryptedPassword = connection.EncryptedPassword;
                    MultipleActiveResultSets = connection.MultipleActiveResultSets;
                    ConnectTimeout = connection.ConnectTimeout;
                    IntegratedSecurity = connection.IntegratedSecurity;
                    Pooling = connection.Pooling;
                }
            }
        }

        private static string GetConnectionType(string useConnectionName, Connections connections)
        {
            foreach (var connection in connections)
            {
                if (!connection.ConnectionName.ToLower().Trim().Equals(useConnectionName.ToLower().Trim())) continue;
                return connection.ConnectionType;
            }

            return null;
        }
    }

    public class Connections : List<Connection>
    {
        private const string DefaultConnectionName = "DefaultConnection";

        public Connection DefaultConnection
        {
            get
            {
                return Find(c =>
                    c.ConnectionName.ToLowerInvariant().Equals(DefaultConnectionName.ToLowerInvariant()));
            }
        }

        public Connection GetConnectionByName(string conn)
        {
            return Find(c => c.ConnectionName.ToLowerInvariant().Equals(conn.ToLowerInvariant()));
        }

        public void ReInitialize()
        {
            foreach (var connection in this) connection.ReInitialize(this);
        }
    }
}